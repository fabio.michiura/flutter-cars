import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {

  TextEditingController _login = TextEditingController();
  TextEditingController _password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
    );
  }

  _body() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _imageLogo(
            "https://www.pikpng.com/pngl/m/62-628425_cars-logo-png-disney-cars-3-logo-clipart.png"),
        _textFormField(
            "Login", "Digite o seu login:", controller: _login),
        SizedBox(
          height: 10,
        ),
        _textFormField("Senha", "Digite a sua senha:", obscureText: true, controller: _password),
        _button("LOGIN"),
      ],
    );
  }

  _imageLogo(String image) {
    return Image.network(
      image,
      width: 200,
      height: 150,
    );
  }

  _textFormField(String label, String hint, {bool obscureText = false, TextEditingController controller}) {
    return TextFormField(
      controller: controller,
      obscureText: obscureText,
      decoration: InputDecoration(
        labelText: label,
        hintText: hint,
        labelStyle: TextStyle(
          color: Colors.red,
          fontSize: 20,
        ),
      ),
    );
  }

  _button(String label) {

    return Container(
      height: 48,
      child: RaisedButton(
        color: Colors.blue,
        child: Text(
          label,
          style: TextStyle(
            color: Colors.white,
            fontSize: 22,
          ),
        ),
        onPressed: _onPressed,
      ),
    );
  }

  _onPressed() {
    print("Login: ${_login.text}");
    print("Senha: ${_password.text}");
  }
}
